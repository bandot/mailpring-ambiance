# Mailpring Ambiance

Modified Ambiance theme for Mailspring that fix window decoration, toolbar background, and color scheme

## Screenshot
![](screenshot/custom-theme.png)

## Installing

* Download the package
* Launch Mailspring app
* Open Edit > Install Theme
* Browse the downloaded package
* Enjoy

## Structure

```
.
├── styles                 # All stylesheets
|   ├── index.less         # Main LESS file to import your stylesheets
│   ├── ui-variables.less  # UI variables that override N1's defaults
├── package.json           # Metadata about the theme
├── LICENSE.md             # License with usage rights
└── README.md              # Info about your theme and how to use it
```

## License

This project is licensed under the MIT - see the [LICENSE.md](LICENSE.md) file for details